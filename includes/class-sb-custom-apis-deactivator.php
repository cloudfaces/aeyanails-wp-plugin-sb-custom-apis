<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://shivbabbar.com
 * @since      1.0.0
 *
 * @package    Sb_Custom_Apis
 * @subpackage Sb_Custom_Apis/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Sb_Custom_Apis
 * @subpackage Sb_Custom_Apis/includes
 * @author     shiv babbar <shivbabbar@hotmail.com>
 */
class Sb_Custom_Apis_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
