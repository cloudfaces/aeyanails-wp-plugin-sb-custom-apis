<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://shivbabbar.com
 * @since      1.0.0
 *
 * @package    Sb_Custom_Apis
 * @subpackage Sb_Custom_Apis/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Sb_Custom_Apis
 * @subpackage Sb_Custom_Apis/includes
 * @author     shiv babbar <shivbabbar@hotmail.com>
 */
class Sb_Custom_Apis_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'sb-custom-apis',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
