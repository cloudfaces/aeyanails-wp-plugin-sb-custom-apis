<?php

/**
 * Fired during plugin activation
 *
 * @link       http://shivbabbar.com
 * @since      1.0.0
 *
 * @package    Sb_Custom_Apis
 * @subpackage Sb_Custom_Apis/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Sb_Custom_Apis
 * @subpackage Sb_Custom_Apis/includes
 * @author     shiv babbar <shivbabbar@hotmail.com>
 */
class Sb_Custom_Apis_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
