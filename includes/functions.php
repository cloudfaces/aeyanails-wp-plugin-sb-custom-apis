<?php



if (!function_exists('pr')) {
    function pr($post){
        echo '<pre>';
            print_r($post);
        echo '</pre>';
    }
}


// Custom rest api
add_action('rest_api_init', function ()
{
    register_rest_route( 'api_crate_new', 'user',array(
  'methods' => 'POST',
  'callback' => 'api_create_new_user'
  ));  // mandatory check done

  register_rest_route( 'api_update_user', 'user',array(
	'methods' => 'POST',
	'callback' => 'update_user'
	));  // mandatory check done

    register_rest_route( 'create_appointment', 'by_logged_in_user',array(
  'methods' => 'POST',
  'callback' => 'create_appointment_by_logged_in_user'
  ));  // mandatory check done



  register_rest_route( 'get_all_bookings', 'currentuser',array(
  'methods' => 'POST',
  'callback' => 'get_all_bookings_currentuser'
  ));  // mandatory check done

  register_rest_route( 'api_logout', 'logout',array(
  'methods' => 'POST',
  'callback' => 'logout_current_user'
  ));  // mandatory check done

  register_rest_route( 'get_currnt_usrid', 'currntuser',array(
  'methods' => 'POST',
  'callback' => 'get_current_user_id_restapi'
  ));  // mandatory check done

});


function get_current_user_id_restapi()
{
	$currentuserid_fromjwt = get_current_user_id();
	    $data_response = array( 'status' => 1, 'message'=>$currentuserid_fromjwt );
	    return new WP_REST_Response( $data_response, 200 );
	    exit;
}




function logout_current_user()
{
	if(is_user_logged_in()) {
      wp_logout();
      $data_response = array( 'status' => 1, 'message'=>'user logout' );
	    return new WP_REST_Response( $data_response, 200 );
	    exit;
     

   }
   else
   {
		$data_response = array( 'status' => 0, 'message'=>'user already logout' );
	    return new WP_REST_Response( $data_response, 200 );
	    exit;
   }
}


function api_create_new_user()
{
	$posted_datapost 		= json_decode( file_get_contents( 'php://input' ), true );

	$allowed_html   		=   array();
    $user_email  			=   trim( wp_kses ($posted_datapost['user_email_register'],$allowed_html ));
    $user_name   			=   trim( wp_kses ($posted_datapost['user_name_register'],$allowed_html ));
	$user_password   		=   trim( wp_kses ($posted_datapost['user_password_register'],$allowed_html ));
	// $auto_login_code        =  wp_generate_password( $length=10)  

   	$user_name .= wp_generate_password( $length=4, $include_standard_special_chars=false );

   
        // if (preg_match("/^[0-9A-Za-z_]+$/", $user_name) == 0) {
        //     $data_response = array( 'status' => 0, 'message'=>'Invalid username (do not use special characters or spaces)!' );
        //     //wc_clear_notices();
        //     return new WP_REST_Response( $data_response, 200 );
        //     exit;
        // }
        
        
        if ($user_email=='' || $user_name==''){
            $data_response = array( 'status' => 0, 'message'=>'Username and/or Email field is empty!' );
            //wc_clear_notices();
            return new WP_REST_Response( $data_response, 200 );
            exit;
        }
        
        if(filter_var($user_email,FILTER_VALIDATE_EMAIL) === false) {
            $data_response = array( 'status' => 0, 'message'=>'The email doesn\'t look right!' );
            //wc_clear_notices();
            return new WP_REST_Response( $data_response, 200 );
            exit;
        }
        
        // $domain = substr(strrchr($user_email, "@"), 1);
        // if( !checkdnsrr ($domain) ){
        //     $data_response = array( 'status' => 0, 'message'=>'The email\'s domain doesn\'t look right!' );
        //     wc_clear_notices();
        //     return new WP_REST_Response( $data_response, 200 );
        //     exit;
        // }
        
        
        //$user_id     =   username_exists( $user_name );
        if ( username_exists( $user_name )  || email_exists( $user_email ) )
        {
            $data_response = array( 'status' => 0, 'message'=>'Username or email already exists.  Please choose a new one.!' );
            //wc_clear_notices();
            return new WP_REST_Response( $data_response, 200 );
            exit;
		}
		
		$my_array['ID'] = $user_id;   
		if ( isset(  $_POST['user_login'] ) ) {
			$my_array['user_login'] =  $_POST['user_login'];
		} 
		else {   
			$user_info = get_userdata( $user_id );
			$my_array['user_login'] = $user_info->user_login;
		}
        


    // Create the new user
	$user_id = wp_create_user( $user_name, $user_password, $user_email );
	
	update_user_auto_login_code($user_id, $user_email);
	

    //mail

	wp_mail( $user_email, 'Welcome to Aeya Nails!', 'You have successfully registered to Aeya Nails. Thank you!' );
	$data = array('status'=>1, 'message'=>'You have successfully registered to Aeya Nails. Thank you!');
    return new WP_REST_Response( $data, 200 );
    exit;



}

function update_user_auto_login_code($user_id = 0, $user_email = '')
{
	global $meta_key;  
	global $my_array;  

	$meta_key = "pkg_autologin_code";
	$my_array['mkey'] = $meta_key;  
	$meta_value = wp_hash($user_email.'@'.time());
	$my_array['kkey'] = $key;

	$user_meta = update_metadata( 'user', $user_id, $meta_key, $meta_value);

	return $meta_value; 
}

function update_user() {
	try {
		$posted_datapost 		= json_decode( file_get_contents( 'php://input' ), true );

		$allowed_html   		=   array();
		$user_id  				=   (int) trim( wp_kses ($posted_datapost['user_id'], $allowed_html ));
		$user_name   			=   trim( wp_kses ($posted_datapost['user_name_register'],$allowed_html ));
		$phone   			=   trim( wp_kses ($posted_datapost['user_phone'],$allowed_html ));

		// Create the new user
		wp_update_user(array(
			'ID' => $user_id,
			'nickname' => $user_name,
			'display_name' => $user_name,
			'first_name' => $user_name,
			'booked_phone' => $phone
		));
		
		$data = array('status' => 1, 'message'=>'Your account has been successfully updated to Aeya Nails. Thank you!');
		return new WP_REST_Response( $data, 200 );
		exit;
	} catch (Exception $e) {
		$data_response = array( 'status' => 0, 'message'=> $e->getMessage() );
		//wc_clear_notices();
		return new WP_REST_Response( $data_response, 200 );
		exit;
	}

}



function create_appointment_by_logged_in_user()
{
	// action: booked_add_appt
	// customer_type: current
	// user_id: 1
	// is_fe_form: true
	// total_appts: 1
	// appoinment: 0
	// calendar_id: 0
	// title: bbbbbbbbbbbb
	// date: 2020-06-24
	// timestamp: 1592960700
	// timeslot: 0105-0115
}


function get_all_bookings_currentuser()
{
	$posted_datapost = json_decode( file_get_contents( 'php://input' ), true );
	// return new WP_REST_Response( [($posted_datapost['user_id'] != $currentuserid_fromjwt)], 200 );
	$currentuserid_fromjwt = get_current_user_id();
	if( (!isset($posted_datapost['user_id'])) || ($posted_datapost['user_id'] != $currentuserid_fromjwt)  || (empty($posted_datapost['user_id'])) )
	{
		$data = array('status' => 2,'message'=>'Please send user id or user is not logged in' );
		return new WP_REST_Response( $data, 200 );
		exit;
		die;
	}



	$currentuserid = get_current_user_id();
	if($currentuserid=='0')
	{
		$data = array('status' => 0,'message'=>'User is not logged in' );
		return new WP_REST_Response( $data, 200 );
		exit;
		die;
	}


	$full_post_data = array();
	$booking_meta_data = array();
		$currentuserid = get_current_user_id();
		$args = array(
		'post_type'     	=> 'booked_appointments',
		'fields'    		=>  'all',
		'author'        	=>  $currentuserid,
		'post_status' 		=>  array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
		'orderby'       	=>  'post_date',
		'order'         	=>  'ASC',
	    'posts_per_page' 	=> -1
	    );

	$current_user_bookings = get_posts( $args );
	foreach($current_user_bookings as $single_booking)
	{
	    $single_booking_ID = $single_booking->ID;
	    $booking_meta_data = get_post_meta($single_booking_ID);
	    $full_post_data[] = array('booking_data'=>$single_booking, 'booking_meta_data'=>$booking_meta_data);
	}


	$data = array('status'=>1, 'booked_appointments'=>$full_post_data);
    return new WP_REST_Response( $data, 200 );
     exit;
}

//add_action('wp_footer','testdatanew');
function testdatanew()
{
	$full_post_data = array();
	$booking_meta_data = array();
		$currentuserid = get_current_user_id();
		$args = array(
		'post_type'     	=> 'booked_appointments',
		'fields'    		=>  'all',
		'author'        	=>  $currentuserid,
		'post_status' 		=>  array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash'),
		'orderby'       	=>  'post_date',
		'order'         	=>  'ASC',
	    'posts_per_page' 	=> -1
	    );

	$current_user_bookings = get_posts( $args );
	foreach($current_user_bookings as $single_booking)
	{
	    $single_booking_ID = $single_booking->ID;
	    $booking_meta_data = get_post_meta($single_booking_ID);
	    $full_post_data[] = array('booking_data'=>$single_booking, 'booking_meta_data'=>$booking_meta_data);
	}


	$data = array('status'=>1, 'booked_appointments'=>$full_post_data);
	echo "<pre>";
    print_r($full_post_data);
}




function jwt_auth_function($data, $user) { 
	$data['user_role'] = $user->roles;
	$data['user_id'] = $user->ID; 
	$data['booked_phone'] = $user->booked_phone;
	$data['auto_login_code'] = update_user_auto_login_code($user->ID, $user->user_email);
	$data['avatar']= get_avatar_url($user->ID);
	return $data; 
} 
add_filter( 'jwt_auth_token_before_dispatch', 'jwt_auth_function', 10, 2 );