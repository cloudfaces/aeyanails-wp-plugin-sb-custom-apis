<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://shivbabbar.com
 * @since             1.0.0
 * @package           Sb_Custom_Apis
 *
 * @wordpress-plugin
 * Plugin Name:       sb custom apis
 * Plugin URI:        http://shivbabbar.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            shiv babbar
 * Author URI:        http://shivbabbar.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       sb-custom-apis
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'SB_CUSTOM_APIS_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-sb-custom-apis-activator.php
 */
function activate_sb_custom_apis() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sb-custom-apis-activator.php';
	Sb_Custom_Apis_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-sb-custom-apis-deactivator.php
 */
function deactivate_sb_custom_apis() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sb-custom-apis-deactivator.php';
	Sb_Custom_Apis_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_sb_custom_apis' );
register_deactivation_hook( __FILE__, 'deactivate_sb_custom_apis' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-sb-custom-apis.php';
require plugin_dir_path( __FILE__ ) . 'includes/functions.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_sb_custom_apis() {

	$plugin = new Sb_Custom_Apis();
	$plugin->run();

}
run_sb_custom_apis();
